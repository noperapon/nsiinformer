require('insert-css')(require('./style.css'))

module.exports = {

    template: require('./template.html'),
    replace: true,
    data: function() {
        return {}
    },
    computed: {
        image: function() {
            return $utils.getAuthorImageByAuthorUrl(this.authorUrl);
        },
        published_moment: function() {
            return moment(new Date(this.published)).calendar();
        },
        showAuthorAsUrl: function(){
            return !!this.authorUrl;
        },
        showAuthorAsSpan: function(){
            return !!!this.authorUrl;
        },
        showAuthorEmail: function(){
            return !!this.email;
        }
    },
    created: function() {

    },
    ready: function() {

    },

    methods: {

    }
}