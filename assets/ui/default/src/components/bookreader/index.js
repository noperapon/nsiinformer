//require('insert-css')(require('./style.css'))

module.exports = {

    template: require('./template.html'),
    replace: true,
    data: function() {
        return {
            bookText: '',
            authorInfo: undefined
        };
    },
    created: function() {},
    ready: function() {
        var self = this;
        this.$on('render_book_text', function(info) {
            this.scrollTop =0;
            self.bookText = info.text;            
            self.authorInfo = info.data;
        });
        if ($states.currentBook) {
            this.bookText = $states.currentBook.text;
            this.authorInfo = $states.currentBook.data;
        }
    },
    attached: function(){
            var scroller = $('#scroller-wrapper-text')[0];
            if (scroller){
                scroller.scrollTop = this.scrollTop;
            }        
    },
    methods: {
        onScroll: function(){
            var scroller = $('#scroller-wrapper-text')[0];
            if (scroller){
                this.scrollTop = scroller.scrollTop;
            }
        }
    }
};