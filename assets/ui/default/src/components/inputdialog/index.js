//require('insert-css')(require('./style.css'))

module.exports = {

    template: require('./template.html'),
    replace: true,
    data: function() {
        return {
            title: 'title',
            input: '',
            description: '',
            success_callback: undefined
        }
    },
    created: function() {
        $states.set('input_dialog_instance', this);
    },
    methods: {
        show: function(params, success_callback) {
            this.input = params.input;
            this.title = params.title;
            this.description = params.description;
            this.success_callback = success_callback;
            $('#DarkNoShadowInputModal').modal('show');
        },
        onOk: function(){
            if (this.success_callback){
                this.success_callback(this.input);
            }
        }
    }
}