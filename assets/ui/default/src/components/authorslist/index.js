require('insert-css')(require('./style.css'))
module.exports = {
    template: require('./template.html'),
    replace: true,
    data: function() {
        return {
            authors: []
        }
    },
    created: function() {
        var self = this;
        var Vue = require('vue');
        Vue.component("author", require('./../author/index.js'));
        db.authors.on('add', function(authorObj) {
            self.updateAuthor(authorObj.toJSON());
        });
        // if author removed
        db.authors.on('remove', function(authorObj) {
            self.removeAuthor(authorObj.toJSON());
        });
        // if nothing local - fetch data from server
        if (db.authors.length === 0) {
            db.authors.fetch();
        } else {
            console.log('>loading authors from local cache');
            _(db.authors.toJSON()).forEach(function(author) {
                self.authors.push(author.toJSON());
            });
        }
        $(window).on('resize', function() {
            self.timer_based_refresh();
        });
    },
    ready: function() {
        var self = this;
        db.authors.on('change', function(authorObj) {
            self.updateAuthor(authorObj.toJSON());
        });
        this.scroller = new IScroll('#ascroller-wrapper', {
            scrollX: false,
            scrollY: true,
            mouseWheel: true,
            scrollbars: true,
            fadeScrollbars: true,
            interactiveScrollbars: true
        });
        self.timer_based_refresh();
    },
    methods: {
        timer_based_refresh: function() {
            if (this.updateLayoutTimer) clearTimeout(this.updateLayoutTimer);
            this.updateLayoutTimer = setTimeout(this.update_layout, 100);
        },
        update_layout: function() {
            this.scroller.refresh();
            theme_init.tooltips();
        },
        updateAuthor: function(author, insertTop) {
            // console.log('>updated');
            //console.log(author);
            //console.log('>searching id ' + author.id + ' in array');
            // console.log(this.authors);
            var index = _.findIndex(this.authors, {
                id: author.id
            });
            //console.log('index = ', index);
            //author.hasUpdates = false;
            if (index >= 0) {
                //console.log('>>>');
                console.log('>updated ' + author.name + ' with "hasUpdates" = ' + author.hasUpdates);
                //this.authors.$set(index, author);
                var localAuthor = this.authors[index];
                for (var attr in author) {
                    if (author.hasOwnProperty(attr)) localAuthor.$set(attr, author[attr]);
                }
            } else {
                if (insertTop) {
                    this.authors.unshift(author);
                } else {
                    this.authors.push(author);
                }
                console.log('>added ' + author.name);
            }
        },
        removeAuthor: function(author) {
            var index = _.findIndex(this.authors, {
                id: author.id
            });
            if (index >= 0) {
                console.log('>removed ' + author.name);
                this.authors.$remove(index);
            }
        },
        toggleAuthorsList: function() {
            $utils.toggleAuthorsList();
        },
        addAuthor: function() {
            var self = this;
            $utils.showInputDialog({
                title: 'Добавление автора',
                description: 'Укажите url автора в виде "http://samlib.ru/p/pupkin_wasja_ibragimowich/"',
                input: ''
            }, function(result) {
                if (!result || !result.trim()) {
                    return $utils.showDialog('Все ж таки надо ввести нормальный URL автора, а то это какая-то профанация искусства программирования!', 'Упс, косяк');
                };
                window.socket.get("/writer/addAuthor", {
                    url: result
                }, function gotResponse(resData, jwres) {
                    if (resData.error) {
                        $utils.showDialog('Ошибка: ' + resData.error, 'Добавление автора');
                    } else {
                        if (resData.writer) {
                            self.updateAuthor(resData.writer, true);
                        }
                    }
                });
            });
        }
    }
}