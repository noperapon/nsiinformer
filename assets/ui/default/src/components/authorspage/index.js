// require('insert-css')(require('./style.css'))

module.exports = {
    template: require('./template.html'),
    replace: true,
    data: function() {
        return {

        }
    },    
    computed:{
       
    },
    created: function() {
        var Vue = require('vue');
        Vue.component("authorslist", require('./../authorslist/index.js'));
        Vue.component("bookslist", require('./../bookslist/index.js'));
    }
}