/**
 * app.js
 *
 * This file contains some conventional defaults for working with Socket.io + Sails.
 * It is designed to get you up and running fast, but is by no means anything special.
 *
 * Feel free to change none, some, or ALL of this file to fit your needs!
 */


(function(io) {

    moment.locale('ru');

    io.socket.on('connect', set_bindings);

    function set_bindings() {
        console.log('>connected to server ');
        // subscribe to models changes 
        io.socket.on("book", function(event) {
            log("book");
            console.log(event);
             if (event.verb == 'updated' || event.verb == 'added') {
                db.books.set([event.data], {
                    remove: false
                });
            } else if (event.verb == 'removed' || event.verb == 'deleted') {
                db.books.remove(event.data);
            }
        });
        io.socket.on("comment", function(event) {
            log("comment");
            log(event);
        });
        io.socket.on("writer", function(event) {
            log('writer');
            console.log(event);
            if (event.verb == 'updated' || event.verb == 'added') {
                db.authors.set([event.data], {
                    remove: false
                });
            } else if (event.verb == 'removed' || event.verb == 'deleted') {
                db.authors.remove(event.data);
            }
        });
        io.socket.on("updatelogitem", function(event) {
            log("log_item");
            log(event);
        });

        io.socket.get("/service/watch", function gotResponse(resData, jwres) {});

        io.socket.on('disconnect', function() {
            io.socket.removeAllListeners('book');
            io.socket.removeAllListeners('comment');
            io.socket.removeAllListeners('writer');
            io.socket.removeAllListeners('updatelogitem');
            io.socket.on('connect', set_bindings);
        });
    }


    // // Expose connected `socket` instance globally so that it's easy
    // // to experiment with from the browser console while prototyping.
    window.socket = io.socket;




    // Simple log function to keep the example simple
    function log() {
        if (typeof console !== 'undefined') {
            console.log.apply(console, arguments);
        }
    }




})(

    // In case you're wrapping socket.io to prevent pollution of the global namespace,
    // you can replace `window.io` with your own `io` here:
    window.io

);