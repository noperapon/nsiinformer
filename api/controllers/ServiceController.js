 module.exports = {

 watch: function(req, res) {
        // subscribe to model creation
        Book.watch(req.socket);
        // subscribe to instances
        // Get all of the Books
        Book.find().exec(function(err, books) {
            // Subscribe the requesting socket (e.g. req.socket) to all books (e.g. books)
            Book.subscribe(req.socket, books);
        });

        // subscribe to model creation
        Comment.watch(req.socket);
        // subscribe to instances
        // Get all of the Books
        Comment.find().exec(function(err, comments) {
            // Subscribe the requesting socket (e.g. req.socket) to all comments (e.g. comments)
            Comment.subscribe(req.socket, comments);
        });

        // subscribe to model creation
        Writer.watch(req.socket);
        // subscribe to instances
        // Get all of the Books
        Writer.find().exec(function(err, writers) {
            // Subscribe the requesting socket (e.g. req.socket) to all writers (e.g. writers)
            Writer.subscribe(req.socket, writers);
        });

        // subscribe to model creation
        UpdateLogItem.watch(req.socket);
        // subscribe to instances
        // Get all of the Books
        // UpdateLogItem.find().exec(function(err, logItems) {
        //     // Subscribe the requesting socket (e.g. req.socket) to all logItems (e.g. logItems)
        //     UpdateLogItem.watch(req.socket, logItems);
        // });

        console.log('->User subscribed to server events');

    }

}