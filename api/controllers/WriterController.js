/**
 * WriterController
 *
 * @description :: Server-side logic for managing writers
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */
module.exports = {
    addCommentsPage: function(req, res) {
        // Send a JSON response
        return res.json({
            hello: 'ADDING'
        });
    },
    getComments: function(req, res) {
        // Send a JSON response
        return res.json({
            hello: 'comments'
        });
    },
    getAll: function(req, res) {
        Writer.find({}).sort({
            updateDetectedAt: -1
        }).exec(function(err, items) {
            if (err) {
                return res.json({
                    error: err
                });
            } else {
                // console.log("returning:");
                // console.log(items);
                return res.json(items);
            }
        });
    },
    toggleCheckUpdates: function(req, res) {
        var query = {};
        if (req.param('id')) {
            query.id = req.param('id');
        } else {
            var err = '[WriterController.toggleCheckUpdates] Error> no writer id supplied';
            console.log(err);
            return res.json({
                error: err
            });
        }
        var checkUpdates = req.param('checkUpdates') == 'true' ? true : false;
        Writer.findOne(query).exec(function(err, writer) {
            if (err) {
                console.log('[WriterController.toggleCheckUpdates] Error> was unable to find Writer object: ' + err);
                return res.json({
                    error: err
                });
            }
            writer.checkUpdates = checkUpdates;
            writer.save(function(err1) {
                if (err1) {
                    console.log('[WriterController.toggleCheckUpdates] Error> was unable to save Writer object: ' + err1);
                    return res.json({
                        error: err1
                    });
                } else {
                    // console.log('> updated Writer ' + writer.id);
                    Writer.publishUpdate(writer.id, writer);
                }
                return res.json({});
            });
        });
    },
    toggleHasUpdates: function(req, res) {
        var query = {};
        var queryBooks = {};
        if (req.param('id')) {
            query.id = queryBooks.writer = req.param('id');
        } else {
            var err = '[WriterController.toggleHasUpdates] Error> no writer id supplied';
            console.log(err);
            return res.json({
                error: err
            });
        }
        var hasUpdates = req.param('hasUpdates') == 'true' ? true : false;
        Writer.findOne(query).exec(function(err, writer) {
            if (err) {
                console.log('[WriterController.toggleHasUpdates] Error> was unable to find Writer object: ' + err);
                return res.json({
                    error: err
                });
            }
            console.log('[WriterController.toggleHasUpdates]  Set  ' + writer.name + ' "has updates" to ' + hasUpdates);
            writer.hasUpdates = hasUpdates;
            writer.save(function(err1) {
                if (err1) {
                    console.log('[WriterController.toggleHasUpdates] Error> was unable to save Writer object: ' + err1);
                    return res.json({
                        error: err1
                    });
                } else {
                    // console.log('> updated Writer ' + writer.id);
                    Writer.publishUpdate(writer.id, writer);
                }
                if (hasUpdates) {
                    return res.json({});
                } else {
                    queryBooks.hasUpdates = !hasUpdates;
                    Book.update(queryBooks, {
                        hasUpdates: hasUpdates
                    }, function(err2, books) {
                        if (err2) {
                            console.log('[WriterController.toggleHasUpdates] Error> was unable to update books: ' + err2);
                            return res.json({
                                error: err2
                            });
                        } else {
                            console.log('[WriterController.toggleHasUpdates] updated ' + books.length + ' books');
                            for (var i = 0; i < books.length; i++) {
                                // console.log('[WriterController.toggleHasUpdates] evented to client ' + books[i].title);
                                Book.publishUpdate(books[i].id, books[i]);
                            };
                            return res.json({});
                        }
                    });
                }
            });
        });
    },
    addAuthor: function(req, res) {
        var timeStamp = function() {
            return '[' + new Date() + '] ';
        }
        if (!req.param('url')) {
            console.log('[WriterController.addAuthor] Error> No author URL supplied!');
            return res.json({
                error: 'No author URL supplied!'
            });
        }
        var url = req.param('url');
        Service.addAuthor(url, function(err, result) {
            if (err) {
                console.log('[WriterController.addAuthor] Error> was unable to add author: ' + err);
                return res.json({
                    error: err
                });
            }
            console.log('[WriterController.addAuthor] Added author ' + url);
            if (result.writer) {
                var writer = result.writer;
                var siteDetector = require('./../lib/siteDetector.js');
                var site = siteDetector.getSiteByTemplate(writer.siteTemplate);
                // set next update time
                writer = site.schedulers.author.calculateNextCheckTime(writer);
                console.log(timeStamp() + '|>WriterController. Author "' + writer.name + '" next time will be checked in ' + writer.checkUpdatesAt);
                // save writer
                site.storage.author.update(writer, function() {
                    Writer.publishUpdate(writer.id, writer);
                    // here Writer.publishUpdate not working, thats why we return added object
                    return res.json({
                        writer: writer
                    });
                });
            }else
                return res.json({});
        });
    },
    checkAuthor: function(req, res) {
        var timeStamp = function() {
            return '[' + new Date() + '] ';
        }
        var query = {};
        if (req.param('id')) {
            query.id = req.param('id');
        } else {
            var err = '[WriterController.checkAuthor] Error> no writer id supplied';
            console.log(err);
            return res.json({
                error: err
            });
        }
        Writer.findOne(query).exec(function(err, writer) {
            if (err) {
                console.log('[WriterController.checkAuthor] Error> was unable to find Writer object: ' + err);
                return res.json({
                    error: err
                });
            }
            Service.checkAuthor(writer.href, function(err, result) {
                if (err) {
                    console.log(timeStamp() + '|>WriterController. ERROR "' + writer.name + '" NOT checked. ' + err);
                } else {
                    if (result.success) {
                        console.log(timeStamp() + '|>WriterController. Author "' + writer.name + '" checked and he ' + ((result.authorHasNewUpdates) ? 'has NEW updates!' : 'has NO new updates'));
                    }
                    if (result.success && result.writer) {
                        var siteDetector = require('./../lib/siteDetector.js');
                        var site = siteDetector.getSiteByTemplate(writer.siteTemplate);
                        // set next update time
                        writer = site.schedulers.author.calculateNextCheckTime(result.writer);
                        console.log(timeStamp() + '|>WriterController. Author "' + writer.name + '" next time will be checked in ' + writer.checkUpdatesAt);
                        // save writer
                        site.storage.author.update(writer, function() {
                            Writer.publishUpdate(writer.id, writer);
                        });
                    }
                }
            });
            return res.json({});
        });
    }
};