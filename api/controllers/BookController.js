/**
 * CommentPageController
 *
 * @description :: Server-side logic for managing commentpages
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
    //http://localhost:1337/Book/loadComments?commentsPath=http://samlib.ru/comment/p/pupkin_wasja_ibragimowich/niktxt&skip=0&take=10&sorting=desc
    loadComments: function(req, res) {
        Service.loadComments(req.param('commentsPath'), req.param('skip'), req.param('take'), req.param('sorting'), function(error, result) {
            if (error) {
                return res.json({
                    error: error
                });
            } else {
                return res.json(result);
            }
        });
    },
    // http://localhost:1337/Book/addCommentsPage?commentsPath=http://samlib.ru/comment/p/pupkin_wasja_ibragimowich/niktxt
    addCommentsPage: function(req, res) {
        Service.addCommentsPage(req.param('commentsPath'), function(error, result) {
            if (error) {
                return res.json({
                    error: error
                });
            } else {
                return res.json(result);
            }
        });
    },
    // http://localhost:1337/Book/downloadComments?commentsPath=http://samlib.ru/comment/p/pupkin_wasja_ibragimowich/niktxt
    downloadComments: function(req, res) {
        Service.downloadComments(req.param('commentsPath'), function(error, result) {
            if (error) {
                return res.json({
                    error: error
                });
            } else {
                return res.json(result);
            }
        });
    },
    getBy: function(req, res) {
        var query = {};
        if (req.param('writer')) {
            query.writer = req.param('writer');
        }
        if (req.param('id')) {
            query.id = req.param('id');
        }
        // console.log('>querying books ');
        // console.log(query);
        Book.find(query).sort({
            group: 1,
            updateDetectedAt: -1
        }).exec(function(err, items) {
            if (err) {
                return res.json({
                    error: err
                });
            } else {
                // console.log("returning:");
                // console.log(items);
                return res.json(items);
            }
        });
    },
    getText: function(req, res) {
        var query = {};
        if (req.param('id')) {
            query.id = req.param('id');
        }
        Book.findOne(query).exec(function(err, book) {
            if (err) {
                return res.json({
                    error: err
                });
            } else {
                var detectorPath = __dirname + '/../lib/siteDetector.js';
                var siteDetector = require(detectorPath);
                var site = siteDetector.detectSite(book.href);
                if (site) {
                    site.processors.book.get(site, function(error, result) {
                        if (error) {
                            return res.json({
                                error: error
                            });
                        } else {
                            return res.json(result);
                        }
                    });
                }

            }
        });

    },
    toggleHasUpdates: function(req, res) {
        var query = {};
        var queryWriter = {};
        if (req.param('id')) {
            query.id = req.param('id');
        } else {
            var err = '[BookController.toggleHasUpdates] Error> no book id supplied';
            console.log(err);
            return res.json({
                error: err
            });
        }

        var hasUpdates = req.param('hasUpdates') == 'true' ? true : false;

        Book.findOne(query).exec(function(err, book) {
            if (err) {
                console.log('[BookController.toggleHasUpdates] Error> was unable to find book object: ' + err);
                return res.json({
                    error: err
                });
            }
            console.log('[BookController.toggleHasUpdates]  Set  ' + book.title + ' "has updates" to ' + hasUpdates);
            book.hasUpdates = hasUpdates;
            book.save(function(err1) {
                if (err1) {
                    console.log('[BookController.toggleHasUpdates] Error> was unable to save book object: ' + err1);
                    return res.json({
                        error: err1
                    });
                } else {
                    Book.publishUpdate(book.id, book);
                }
                queryWriter.writer = book.writer;
                queryWriter.hasUpdates = !hasUpdates;
                Book.find(queryWriter).exec(function(err, books) {
                    if (err) {
                        console.log('[BookController.toggleHasUpdates] Error> was unable to load books with updates: ' + err);
                        return res.json({
                            error: err
                        });
                    }
                    if (!books || books.length == 0) {
                        Writer.update({
                            id: book.writer
                        }, {
                            hasUpdates: hasUpdates
                        }, function(err2, writers) {
                            if (err2) {
                                console.log('[BookController.toggleHasUpdates] Error> was unable to update writer object: ' + err2);
                                return res.json({
                                    error: err2
                                });
                            } else {
                                if (writers.length > 0) {
                                    Writer.publishUpdate(writers[0].id, writers[0]);
                                }
                                return res.json({});
                            }
                        });
                    } else {
                        console.log('[BookController.toggleHasUpdates]> found ' + books.length + ' updated books for writer id ' + book.writer);
                        return res.json({});
                    }
                });


            });
        });
    },
};