/**
 * Comment.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/#!documentation/models
 */

module.exports = {

	connection: 'someMongodbServer',

	attributes: {
		html: 'string',
		number: 'integer',
		archive: 'integer',		
		email: 'string',
		author: 'string',
		authorUrl: 'string',
		published: 'string',
		imageUrl: 'string',
		imagePath: 'string',
		commentHtml: 'string',
		comment: 'string',
		editUrl: 'string',
		deleteUrl: 'string',
		replyUrl: 'string',		
		book: {
			model: 'Book'
		}
	}
};