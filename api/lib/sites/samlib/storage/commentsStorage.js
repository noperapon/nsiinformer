// middleware to save data to any storage
// here we use sailjs storage as defined in settings (see database adapters settings)
// but we can use our custom storage
// for commentsStorage we must exports save and loadComments functions:
// exports.save = save; // save book or comments page with comments
// exports.load = load; // load book 
// exports.loadBy = loadBy; // load book with parameters
// exports.loadComments = loadComments; // load array of comments
// exports.update = update; // update book object
/////////////////////////////////////////////////////////////////
// service db functions
/////////////////////////////////////////////////////////////////
// remove page from url and set author page to download and parse it
function toAuthorUrl(url, site) {
    return Utils.stripUrl(url) + '/' + site.authorPageName;
}
// saves book+comments object
function save(bookInfo, site, callback) {
    Book.findOrCreate({
        path: bookInfo.path,
        siteTemplate: bookInfo.siteTemplate
    }).populate('writer').exec(function(err, book) {
        if (err) {
            return callback('[commentsStorage] Error> ' + err);
        }
        if (!book) {
            return callback('[commentsStorage] Error> ' + 'was unable to create book object');
        }
        var oldSize = book.size;
        var oldTitle = book.title;
        var oldAnnotationHash = book.annotationHash;
        var newAnnotationHash = Utils.hashAnnotation(bookInfo.annotation);
        book.site = bookInfo.site;
        book.siteTemplate = bookInfo.siteTemplate;
        book.path = bookInfo.path || book.path;
        book.href = bookInfo.href || book.href;
        book.commentsPath = bookInfo.commentsPath || book.commentsPath;
        book.commentsHref = bookInfo.commentsHref || book.commentsHref;
        book.title = bookInfo.title;
        book.size = bookInfo.size || book.size;
        book.annotation = bookInfo.annotation;
        book.annotationHtml = bookInfo.annotationHtml;
        book.annotationHash = newAnnotationHash;
        book.rating = bookInfo.rating || book.rating;
        book.group = bookInfo.group || book.group;
        book.lastCommentNumber = book.lastCommentNumber || 0;
        book.lastCommentArchive = book.lastCommentArchive || 0;
        book.commentsPages = bookInfo.pages || book.commentsPages;
        book.commentsArchives = bookInfo.archives || book.commentsArchives;
        book.categories = bookInfo.categories || book.categories;
        if (bookInfo.checkComments !== undefined && bookInfo.checkComments !== null) book.checkComments = bookInfo.checkComments;
        var commentsHasUpdates = false;
        var bookHasUpdates = (oldSize && oldTitle) && ((oldTitle !== bookInfo.title)); // (oldSize && oldTitle)- skip just added book           
        bookHasUpdates = bookHasUpdates || (oldAnnotationHash !== newAnnotationHash)
        if (!bookHasUpdates) // convert undefined and null to false
            bookHasUpdates = false;
        // if we detect new updates, remember date-time
        if (bookHasUpdates) {
            book.updateDetectedAt = new Date();
            // TODO set author updated status!
            // writer.updateDetectedAt = book.updateDetectedAt;
            // writer.hasUpdates = true;
            book.prevSize = oldSize;
        }
        var annotationUpdated = oldAnnotationHash && (oldAnnotationHash !== newAnnotationHash);
        if (!annotationUpdated) // convert undefined and null to false
            annotationUpdated = false;
        if (bookInfo.comments) {
            var sortedComments = bookInfo.comments.sort(function(a, b) {
                return (a.number > b.number) ? 1 : ((b.number > a.number) ? -1 : 0);
            });
            sortedComments.forEach(function(c) {
                if (c.archive > book.lastCommentArchive || c.number > book.lastCommentNumber) {
                    book.comments.add(c);
                    book.lastCommentNumber = c.number;
                    book.lastCommentArchive = c.archive;
                    commentsHasUpdates = true;
                }
            });
        }
        var save = function(obj, cb) {
            obj.save(function(err) {
                var result = {};
                if (err) {
                    var e = '[commentsStorage] Error (' + (bookInfo.href || bookInfo.commentsHref) + ')> ' + err;
                    console.log(e);
                    result.success = false;
                    result.error = e;
                } else {
                    console.log('[commentsStorage] SUCCESS saving ' + obj.path);
                    result.success = true;
                    result.commentsHasUpdates = commentsHasUpdates;
                    result.bookHasUpdates = bookHasUpdates;
                    result.annotationUpdated = annotationUpdated;
                    if (obj.title) {
                        result.book = obj;
                    }
                }
                return cb(null, result);
            });
        };
        book.hasUpdates = book.hasUpdates || bookHasUpdates;
        book.commentsHasUpdates = book.commentsHasUpdates || commentsHasUpdates;
        if (commentsHasUpdates || !book.commentsUpdateDetectedAt) {
            book.commentsUpdateDetectedAt = new Date();
        }
        if (!book.checkCommentsAt) book.checkCommentsAt = new Date(); // for search
        if (!book.writer) {
            var writerPath = Utils.stripUrl(book.path);
            var writerUrl = toAuthorUrl(book.href, site);
            Writer.findOrCreate({
                path: writerPath
            }).exec(function(err, writer) {
                if (!err) {
                    if (err) {
                        return callback('[commentsStorage] Error> ' + err);
                    }
                    if (!writer) {
                        return callback('[commentsStorage] Error> ' + 'was unable to create WRITER object');
                    }
                    site.urlParts.href = writerUrl;
                    site.processors.author.get(site, function(error, result) {
                        //console.log('got author: ');
                        if (!error) {
                            writer.site = book.site;
                            writer.siteTemplate = book.siteTemplate;
                            writer.path = writerPath;
                            writer.href = writerUrl;
                            writer.name = result.name;
                            writer.imageUrl = result.image;
                            writer.hasUpdates = writer.hasUpdates || bookHasUpdates;
                            writer.books.add(book);
                            save(writer, function(err, result) {
                                save(book, callback);
                            });
                        } else {
                            return callback('[commentsStorage] Error> ' + error);
                        }
                    });
                }
            });
        } else {
            save(book, callback);
        }
    });
}
// load comments of book, skip comments, take comments
function loadComments(commentsPath, skipComments, takeComments, sorting, callback) {
    //console.log('|> searching for ' + commentsPath);
    if (!sorting) sorting = 'asc';
    if (!skipComments && skipComments !== 0) skipComments = 0;
    if (!takeComments && takeComments !== 0) takeComments = 10;
    Book.findOne({
        commentsPath: commentsPath
    }).
    exec(function(error, book) {
        if (error) {
            return callback(error);
        }
        if (!book) {
            return callback("Book not found. Comments path: " + commentsPath);
        }
        Comment.find().
        where({
            book: book.id,
            archive: book.lastCommentArchive
        }).
        sort("id " + sorting).
        skip(skipComments).
        limit(takeComments).
        exec(function(error, comments) {
            callback(error, comments);
        });
    });
}
// load book
// example: /p/pupkin_vasia_ibragimovich/updatetxt.shtml
function load(bookPath, site, callback) {
    Book.findOne({
        path: bookPath,
        siteTemplate: site.siteTemplate
    }).
    exec(function(error, book) {
        if (error) {
            return callback(error);
        }
        if (!book) {
            return callback("Book not found. Path: " + bookPath);
        }
        callback(error, book);
    });
}

function loadBy(params, callback) {
    Book.find(params).exec(function(err, books) {
        if (err) {
            return callback('[commentsStorage] Error> ' + err);
        }
        if (!books) {
            return callback('[commentsStorage] Error> ' + 'was unable to find Books by params:' + JSON.stringify(params));
        }
        callback(err, books);
    });
}
// update existing Book object
function update(book, callback) {
    // native book object from Waterline ORM model has its own save method
    book.save(function(err) {
        if (err) {
            var e = '[commentsStorage] Error updating (' + (book.href) + ')> ' + err;
            console.log(e);
            return callback(e);
        } else {
            console.log('[commentsStorage] SUCCESS saving updated ' + book.path);
        }
        return callback();
    });
}
exports.save = save; // save book or comments page with comments
exports.load = load; // load book 
exports.loadBy = loadBy; // load book with parameters
exports.loadComments = loadComments; // load array of comments
exports.update = update; // update book object