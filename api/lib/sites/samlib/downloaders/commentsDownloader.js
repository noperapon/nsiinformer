var http = require('http');
var iconv = require('iconv-lite');
var zlib = require('zlib');

exports.download = function(urlParts, cookies, completed) {

    
    var _getCommentsPage = function(urlParts, cookies, completed) {



        var options = {
            hostname: urlParts.hostname,
            port: 80,
            path: urlParts.path,
            method: 'GET',
            headers: {
                "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
                "User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.143 Safari/537.36",
                "Accept-Encoding": "gzip,deflate,sdch",
                "Accept-Language": "ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4,sk;q=0.2",
                "Pragma": "no-cache",
                "Cookie": cookies
            }

        };

        var page = "";

        var req = http.request(options, function(res) {
            if (302 == res.statusCode) {
                cookies = res.headers["set-cookie"];
                _getCommentsPage(urlParts, cookies, completed);
                return;
            } else
            if (200 == res.statusCode) {
                var chunks = [];
                res.on('data', function(chunk) {
                    chunks.push(chunk);
                });
                res.on('end', function() {
                    var encoding = res.headers['content-encoding'];
                    if (encoding && encoding === 'gzip') {
                        var buf = Buffer.concat(chunks);
                        zlib.unzip(buf, function(err, res) {
                            completed(err, iconv.decode(res, 'koi8-r').replace(/\n/igm, ''));
                        });
                    } else {
                        var decodedBody = iconv.decode(Buffer.concat(chunks), 'koi8-r');
                        completed(null, decodedBody.replace(/\n/igm, ''));
                    }
                });
            } else {
                completed("Error getting page. Status code=" + res.statusCode );
            }
        });

        req.on('error', function(e) {
            console.log('problem with request: ' + e.message);
        });

        req.end();

    };

    _getCommentsPage(urlParts, cookies, completed);

};