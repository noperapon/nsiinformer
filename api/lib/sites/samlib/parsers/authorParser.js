var cheerio = require('cheerio');
var sys = require('sys');

function parse(page, urlParts, completed){
    $ = cheerio.load(page);
    var author={};
    
   

    var h3 = $('h3');
    var authorName = $(h3).text().split(":")[0];
    author.name = authorName;
    author.path = urlParts.pathname;
    author.href = urlParts.protocol + '//' + urlParts.host + urlParts.pathname + '/' + urlParts.page;
    author.site = urlParts.hostname;
    author.siteTemplate = urlParts.siteTemplate;
    author.image="";
    author.texts = [];

    // domain relative picture
    var images = $('img');
    var firstImage=$(images).first().attr('src');
    if (firstImage && firstImage.substring(0,7)!='http://')
        author.image = firstImage;


    var textIndex=0;
    var linkItems = $('dl dl dt li a');
    $(linkItems).each(function(i, link){        
        var text = {};
        text.name = $(link).text();
        text.link = $(link).attr('href');
        text.size = $(link).next().text();
        
        var annotation = $(link).next().next().next().next();
        text.annotation = annotation.children('font').text();
        text.annotationHtml = annotation.children('font').html();

        var ratingAndComments = $(link).next().next();
        text.rating  = ratingAndComments.children('b').text();
        // text group
        var params = ratingAndComments.text();
        var indexGroup = params.indexOf('"');
        var indexEndGroup=-1;
        if (indexGroup>=0){
            indexEndGroup = params.indexOf('"', indexGroup+1);
            if (indexEndGroup>=0){
                text.group = params.substring(indexGroup+1, indexEndGroup).trim();
                if (text.group && (text.group.substring(0,1)=="@" || text.group.substring(0,1)=="*"))
                    text.group = text.group.substring(1);
                }
            }

        // text type
        text.categories=[];
        if (indexEndGroup==-1) indexEndGroup = 0;
        var commentsStart = params.indexOf('Комментарии', indexEndGroup+1);
        if (commentsStart==-1) commentsStart = params.length;
        var categoriesText = params.substring(indexEndGroup+1, commentsStart).trim();
        text.categories = categoriesText.split(/\s*,\s*/); // split ',' and trim

        // comments
        text.comments = {};
        text.comments.description = ratingAndComments.children('a').text();
        text.comments.path = ratingAndComments.children('a').attr('href');
        if (text.size && text.name!=text.link && text.link.substring(0, 7) != "http://" && text.link.substring(0, 8) != "https://"){            
                author.texts[textIndex]=text;
                textIndex+=1;
                }
    });
   completed(null, author);
}


exports.parse = parse;
