Date.prototype.addHours = function(h) {    
   this.setTime(this.getTime() + (h*60*60*1000)); 
   return this;   
}

Date.prototype.addMinutes = function(h) {    
   this.setTime(this.getTime() + (h*60*1000)); 
   return this;   
}

Date.prototype.addSeconds = function(h) {    
   this.setTime(this.getTime() + (h*1000)); 
   return this;   
}

if (typeof String.prototype.startsWith != 'function') {
  String.prototype.startsWith = function (str){
    return this.slice(0, str.length) == str;
  };
}

if (typeof String.prototype.endsWith != 'function') {
  String.prototype.endsWith = function (str){
    return this.slice(-str.length) == str;
  };
}