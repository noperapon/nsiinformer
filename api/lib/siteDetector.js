var url = require('url');

var templates = {
	'samlib.ru': 'samlib',
	'budclub.ru': 'samlib',
	'zhurnal.lib.ru': 'samlib',
};

var authorPageName = {
	'samlib': 'indextitle.shtml'
};

var prepareAuthor = {
	'samlib': function(url) {
		var _url = url.toLowerCase();
		if (_url.indexOf('.shtml') < 0 || _url.indexOf('.shtml') !== (_url.length - 6)) {
			if (_url.lastIndexOf('/')===_url.length-1){
				url = url.substring(0, _url.length-1);
			}
			return url;
		}
		var lastSlash = _url.lastIndexOf('/');
		var path = url.substring(0, lastSlash);
		return path;
	}
};

function composeModules(template, urlParts){	
	var root = __dirname + '/sites/';
	return {
		parsers: {
			author: require(root + template + '/parsers/authorParser.js'),
			comments: require(root + template + '/parsers/commentsParser.js'),			
			book: require(root + template + '/parsers/bookParser.js'),			
		},
		downloaders: {
			author: require(root + template + '/downloaders/authorDownloader.js'),
			comments: require(root + template + '/downloaders/commentsDownloader.js'),
			book: require(root + template + '/downloaders/bookDownloader.js'),
		},
		storage: {
			author: require(root + template + '/storage/authorStorage.js'),
			comments: require(root + template + '/storage/commentsStorage.js'),
		},
		processors: {
			author: require(root + template + '/processors/authorProcessor.js'),
			comments: require(root + template + '/processors/commentsProcessor.js'),
			book: require(root + template + '/processors/bookProcessor.js'),
		},
		schedulers: {
			author: require(root + template + '/schedulers/authorScheduler.js'),
			comments: require(root + template + '/schedulers/commentsScheduler.js'),
		},
		urlParts: urlParts,
		authorPageName: authorPageName[template],
		siteTemplate: template,
	};
}

function detectSite(uri) {
	var urlParts = url.parse(uri);
	var hostname = urlParts.hostname;
	if (!hostname) return undefined;
	var template = templates[hostname];
	if (!template) return undefined;
	var modules = composeModules(template, urlParts);	
	if (prepareAuthor[template]) {
		modules.urlParts.pathname = prepareAuthor[template](modules.urlParts.pathname);
		modules.urlParts.path = prepareAuthor[template](modules.urlParts.path);
		modules.urlParts.href = prepareAuthor[template](modules.urlParts.href) + '/' + modules.authorPageName;
		modules.urlParts.page = modules.authorPageName;
		modules.urlParts.siteTemplate = template;
		modules.urlParts.originalUrl = uri;
	}
	return modules;
}

function getStorages() {
	
	var root = __dirname + '/sites/';

	var storages=[];
	var existingTemplates=[];

	for (var name in templates) {
		var tmpl = templates[name];
		if (existingTemplates.indexOf(tmpl)<0)
			existingTemplates.push(tmpl);
	}
	existingTemplates.forEach(function(t){
		storages.push({
			siteTemplate: t,
			author: require(root + t + '/storage/authorStorage.js'),
			comments: require(root + t + '/storage/commentsStorage.js'),
		});
	});
	return storages;
}

function getSiteByTemplate(template){
	return composeModules(template);
}

exports.detectSite = detectSite;
exports.getStorages = getStorages;
exports.getSiteByTemplate = getSiteByTemplate;