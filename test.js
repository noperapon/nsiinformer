//var http = require('follow-redirects').http;
var http = require('http');
var iconv = require('iconv-lite');
var cheerio = require('cheerio');
var util = require('util');
var zlib = require('zlib');
var datejs = require('datejs');

//var request = require('request');

// var url = require('url');

// function request(address, path) {
//     http.get({ host: address, path: path}, function(response) {
//         // The page has moved make new request
//         if (response.statusCode === 302) {
//             var newLocation = url.parse(response.headers.location).host;
//             console.log('We have to make new request ' + newLocation);
//             request(newLocation);
//         } else {
//             console.log("Response: %d", rehttps://vk.com/mustangwantedhttps://vk.com/mustangwantedsponse.statusCode);
//             response.on('data', function(chunk) {
//                 console.log('Body ' + chunk);
//             });
//         }
//     }).on('error', function(err) {
//         console.log('Error %s', err.message);
//     });
// }

// request('http://samlib.ru', '/comment/p/pupkin_wasja_ibragimowich/updatetxt');


// request('http://samlib.ru/comment/p/pupkin_wasja_ibragimowich/updatetxt', function (error, response, body) {
// 	if (error){
// 		console.log(error);
// 	}
//     if (!error && response.statusCode === 200) {
//         console.log(body);
//     }
//     console.log(response.statusCode);
// });


function getPage(hostname, path, cookies, completed) {

    //var url = 'http://' + hostname + '/' + path;
    var options = {
        hostname: hostname,
        port: 80,
        path: path,
        method: 'GET',
        headers: {
            "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
            "User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.143 Safari/537.36",
            "Accept-Encoding": "gzip,deflate,sdch",
            "Accept-Language": "ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4,sk;q=0.2",
            "Pragma": "no-cache",
            "Cookie": cookies
        }
        // "Accept-Charset": "koi8-r",
    };

    var page = "";

    var req = http.request(options, function(res) {
        if (302 == res.statusCode) {
            cookies = res.headers["set-cookie"];
            console.log('cookies: ' + cookies);
            getPage(hostname, path, cookies, completed);
        } else
        if (200 == res.statusCode) {

            var chunks = [];
            res.on('data', function(chunk) {
                chunks.push(chunk);
            });
            res.on('end', function() {
                var encoding = res.headers['content-encoding'];
                if (encoding && encoding === 'gzip') {
                	var buf = Buffer.concat(chunks);
                	zlib.unzip(buf, function(err, res){
	                    completed(null, iconv.decode(res, 'koi8-r'));                		
                	});
                } else {
                    var decodedBody = iconv.decode(Buffer.concat(chunks), 'koi8-r');
                    console.log(decodedBody);
                    completed(null, decodedBody);
                }
            });

        } else {
            completed("Error getting page. Status code=" + res.statusCode);
        }
    });

    req.on('error', function(e) {
        console.log('problem with request: ' + e.message);
    });

    req.end();
}

var cheerio = require('cheerio');


function parse(page, url, callback) {
    $ = cheerio.load(page);
    var pageData = {};
    pageData.url = url;
    // title
    var title = $('title');
    pageData.title = $(title).text().split(":")[1].trim();
    // rating
    var linkItems = $('b a').first();
    pageData.rating = $(linkItems).text();
    // annotation
    var items = $('li i').first();
    pageData.annotation = $(items).text();
    pageData.annotationHtml = $(items).html();
    // pages
    var expr = /<a href=\/comment\/.*?\?PAGE=(\d+)>/igm;
    items = expr.exec(page);
    pageData.pages = 1;
    $(items).each(function(i, item) {
        var p = parseInt(item);
        if (p > pageData.pages)
            pageData.pages = p;
    });
    //comments
    expr = /<small>(\d+)\.<\/small>\s*<b>\*?(?:<noindex><a href=\"(.*?)\"[^>]*>)?(?:<font[^>]*>)?(.*?)(?:<\/font>)?(?:<\/a><\/noindex>)?<\/b>\s*(?:\(<u>(.*?)<\/u>\)\s*)?<small><i>(.*?)\s*<\/i>\s*(?:\[<a href=\"(\S+?)\"\s*>исправить<\/a>\]\s*)?(?:\[<a href=\"(\S+?)"\s*><font[^>]*>удалить<\/font><\/a>\]\s*)?(?:\[<a href=\"(\S+?)"\s*>ответить<\/a>\]\s*)?<\/small><\/i>\s*<br>(.*?)\s*<hr noshade>/igm;

    pageData.comments = [];
    page.replace(expr, function(p0, p1, p2, p3, p4, p5, p6, p7, p8, p9) {
        pageData.comments.push({
            html: p0,
            number: p1,
            authorUrl: p2,
            author: p3,
            email: fromCharString2String(p4),
            published: p5,
            editUrl: p6,
            deleteUrl: p7,
            replyUrl: p8,
            commentHtml: p9,
            comment: html2String(p9)
        });
    });



    callback(null, pageData);
}

function html2String(data) {
    var newData = data.replace(/(<br>)/igm, ' \n');
    newData = newData.replace(/(<([^>]+)>)/igm, "");
    newData = newData.replace(/&nbsp;/igm, " ");
    newData = newData.replace(/&gt;/igm, '\>');
    newData = newData.replace(/&lt;/igm, '\<');
    return newData;
}


function fromCharString2String(data) {
    if (!data) return '';
    var chars = data.split('&#');
    var result = '';
    for (var i = 0; i < chars.length; i++) {
        var ch = chars[i];
        if (ch.trim()) {
            result = result + String.fromCharCode(ch.trim());
        }
    }
    return result;
}

function getAuthorPage(url, completed) {
    var page = "";
    http.get(url, function(res) {
        if (200 == res.statusCode) {
            var chunks = [];
            res.on('data', function(chunk) {
                chunks.push(chunk);
            });
            res.on('end', function() {
                var decodedBody = iconv.decode(Buffer.concat(chunks), 'win1251');
                //console.log(decodedBody);
                completed(null, decodedBody);
            });
            // res.setEncoding("binary");
            // res.on('data', function (chunk) {
            //     var body = iconv.decode(chunk, 'win1251');
            //     page=page+body;                
            //     });
            // res.on('end', function(){
            //     completed(null, page);            
            //     });
        } else {
            completed("Error getting page");
        };
    }).on('error', function(e) {
        completed("Got error: " + e.message);
    });
};

function parseAuthorPage(page, url, completed) {
    $ = cheerio.load(page);
    var author = {};



    var h3 = $('h3');
    var authorName = $(h3).text().split(":")[0];
    author.name = authorName;
    author.url = url;
    author.image = "";
    author.texts = [];

    // domain relative picture
    var images = $('img');
    var firstImage = $(images).first().attr('src');
    if (firstImage && firstImage.substring(0, 7) != 'http://')
        author.image = firstImage;


    var textIndex = 0;
    var linkItems = $('dl dl dt li a');
    $(linkItems).each(function(i, link) {
        var text = {};
        text.name = $(link).text();
        text.link = $(link).attr('href');
        text.size = $(link).next().text();

        var annotation = $(link).next().next().next().next();
        text.annotation = annotation.children('font').text();

        var ratingAndComments = $(link).next().next();
        text.rating = ratingAndComments.children('b').text();
        // text group
        var params = ratingAndComments.text();
        var indexGroup = params.indexOf('"');
        var indexEndGroup = -1;
        if (indexGroup >= 0) {
            indexEndGroup = params.indexOf('"', indexGroup + 1);
            if (indexEndGroup >= 0) {
                text.group = params.substring(indexGroup + 1, indexEndGroup).trim();
                if (text.group && (text.group.substring(0, 1) == "@" || text.group.substring(0, 1) == "*"))
                    text.group = text.group.substring(1);
            }
        }

        // text type
        text.categories = [];
        if (indexEndGroup == -1) indexEndGroup = 0;
        var commentsStart = params.indexOf('Комментарии', indexEndGroup + 1);
        if (commentsStart == -1) commentsStart = params.length;
        var categoriesText = params.substring(indexEndGroup + 1, commentsStart).trim();
        text.categories = categoriesText.split(/\s*,\s*/); // split ',' and trim

        // comments
        text.comments = {};
        text.comments.description = ratingAndComments.children('a').text();
        text.comments.url = ratingAndComments.children('a').attr('href');
        if (text.size && text.name != text.link && text.link.substring(0, 7) != "http://" && text.link.substring(0, 8) != "https://") {
            author.texts[textIndex] = text;
            textIndex += 1;
        }
    });
    completed(null, author);
}



// getAuthorPage("http://samlib.ru/p/pupkin_wasja_ibragimowich/indextitle.shtml", function(err, data){
// 	if (err)
// 		console.log(err);
// 	else{
// 		//console.log(data);
// 		parseAuthorPage(data, 'url', function(err,result){
// 			console.log(util.inspect(result, false, null));
// 		});
// 	}
// });

// getPage('samlib.ru', '/comment/p/pupkin_wasja_ibragimowich/updatetxt', '', function(error, data) {
//     if (error) {
//         console.log('ERROR: ' + error);
//     } else {
//         //console.log(data);
//         data = data.replace(/\n/igm, '');
//         parse(data, 'this is url', function(error, result) {
//             if (error) {
//                 console.log('>>ERROR ' + error);
//             } else {
//                 console.log('RESULT:');
//                 console.log('+++++++++++++++++++++++++++++++++');
//                 console.log(util.inspect(result, false, null));
//             }
//         });
//     }
// });

// var detectorPath = __dirname + '/api/lib/siteDetector.js';
// //console.log(detectorPath);
//  var siteDetector = require(detectorPath);
//  console.log(siteDetector.getStorages());
//  var storages = siteDetector.getStorages();
//  var writers = [];
//  storages.forEach(function(s){
//     s.author.loadBy({
//         checkUpdates: true
//     }, function(err, lwriters){
//         Array.prototype.push.apply(writers, lwriters);
//     });
//  });
//  console.log(writers);
 //console.log(siteDetector.getSiteByTemplate('samlib'));
// var site = siteDetector.detectSite("http://samlib.ru/p/pupkin_wasja_ibragimowich/indextitle.shtml");
// var uri = 'comment/p/pupkin_wasja_ibragimowich/updatetxt';
// //getPage('samlib.ru', uri, '', function(error, data) {
// site.downloaders.comments.download('samlib.ru', uri, '', function(error, data) {
//     if (error) {
//         console.log('ERROR: ' + error);
//     } else {       
//         //site.parsers.comments.
//     	parse(data, uri, function(error, result) {
//             if (error) {
//                 console.log('>>ERROR ' + error);
//             } else {
//                 console.log('RESULT:');
//                 console.log('+++++++++++++++++++++++++++++++++');
//                 console.log(util.inspect(result, false, null));
//             }
//         });  
//     }
// });


var n = new Date();
var n1 = new Date();
n1.addSeconds(10);
console.log(n);
console.log(n1);
console.log(Date.compare(n,n1));